import { Injectable, isDevMode } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Coords } from '../structures/coords.structures';
import { map } from 'rxjs/operators';
import { Weather } from '../structures/weather.structure';

@Injectable({
  providedIn: 'root'
})
export class CurrentWeatherService {

  public weahterSubject: Subject<any> = new Subject<any>();
  public weather$ : Observable<any> = this.weahterSubject.asObservable();

  endpoint: string = 'https://api.openweathermap.org/data/2.5/weather'

  constructor( private http: HttpClient ) {

    this.weather$ = this.weahterSubject.asObservable().pipe(
      map((data: any)=>{

        let mainWeather = data.weather[0]

        let weather: Weather = {
          name: data.name,
          cod: data.cod,
          temp: data.main.temp,
          ...mainWeather
        }

        return weather;
     })
    );

    this.get({
      lat: 40.416775,
      lon: -3.703790
    });
  }

  get(coords: Coords){

    let args : string = `?lat=${coords.lat}&lon=${coords.lon}&appid=${environment.key}&units=metric`;

    let url = this.endpoint + args;

    if(isDevMode()){
      url = 'assets/weather.json';
    }

    this.http.get(url).subscribe(this.weahterSubject);

  }

}
