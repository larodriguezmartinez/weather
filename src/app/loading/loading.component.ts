import { Component, OnInit } from '@angular/core';
import { loadingAnimation } from '../animations/loading.animations';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.sass'],
  animations: [loadingAnimation()],
})
export class LoadingComponent implements OnInit {

  _elements : string[] = ['#203048' , '#E07918' , '#E07918' , '#203048'];
  public elements : string[];

  constructor() { }

  ngOnInit(){
    this.set();

  }

  set(){
    this.elements = this._elements;
    this. scheduleNextIterarion();
  }

  scheduleNextIterarion(){

    setTimeout(() => {
      if(this.elements.length == 0){

        this.set();

      }else{

        this.clear();
      }

    }, 100 * this._elements.length + 300);

  }

  clear(){
    this.elements = [];
    this. scheduleNextIterarion();
  }
}



