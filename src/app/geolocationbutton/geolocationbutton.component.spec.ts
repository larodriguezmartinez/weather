import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeolocationbuttonComponent } from './geolocationbutton.component';

describe('GeolocationbuttonComponent', () => {
  let component: GeolocationbuttonComponent;
  let fixture: ComponentFixture<GeolocationbuttonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeolocationbuttonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeolocationbuttonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
