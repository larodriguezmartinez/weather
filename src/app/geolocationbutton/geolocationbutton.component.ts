import { Component, OnInit } from '@angular/core';
import { GeolocationService } from '../services/geolocation.service';

@Component({
  selector: 'app-geolocationbutton',
  templateUrl: './geolocationbutton.component.html',
  styleUrls: ['./geolocationbutton.component.sass']
})
export class GeolocationbuttonComponent implements OnInit {

  active : boolean = false;
  constructor(private geolocationService: GeolocationService) { }

  ngOnInit() {

    this.geolocationService.permission$.then((status) => {
      this.active = (status == 'granted')

      if(this.active){
        this.geolocationService.requestGeolocation();
      }

    });
  }
}
